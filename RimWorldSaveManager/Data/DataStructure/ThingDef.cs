﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RimWorldSaveManager.Data.DataStructure
{
    public class ThingDef
    {
        private string _ClassName;
        private string _ParentName;
        private string _DefName;
        private string _Label;
        private string _Category;
        private int? _StackMax;
        private string[] _ThingCategories;

        public ThingDef(XElement xElement)
        {
            _ClassName = xElement.Attribute("Name").GetValue();
            _ParentName = xElement.Attribute("ParentName")?.GetValue();

            string parentClass = _ParentName;
            while (parentClass != null && DataLoader.BaseThings.ContainsKey(parentClass))
            {
                var thing = DataLoader.BaseThings[parentClass];
                if (thing._DefName != null)
                    _DefName = thing._DefName;
                if (thing._Label != null)
                    _Label = thing._Label;
                if (thing._Category != null)
                    _Category = thing._Category;
                if (thing._StackMax != null)
                    _StackMax = thing._StackMax;

                var array2 = thing._ThingCategories;
                if (array2 != null)
                {
                    if (_ThingCategories != null)
                    {
                        _ThingCategories = _ThingCategories.Concat(array2).ToArray();
                    }
                    else
                    {
                        _ThingCategories = array2;
                    }
                }

                parentClass = thing._ParentName;
            }

            if (_DefName == null)
                _DefName = xElement.Element("defName")?.GetValue();
            if (_Label == null)
                _Label = xElement.Element("label")?.GetValue();
            if (_Category == null)
                _Category = xElement.Element("category")?.GetValue();
            if (_StackMax == null)
                _StackMax = xElement.Element("stackLimit")?.GetValue<int>();

            var array = xElement.Element("thingCategories")?.Elements("li").Select(e => e.Value).ToArray();
            if (array != null)
            {
                if (_ThingCategories != null)
                {
                    _ThingCategories = _ThingCategories.Concat(array).ToArray();
                }
                else
                {
                    _ThingCategories = array;
                }
            }
        }

        public string DefName { get => _DefName; }
        public string Label { get => _Label; }

        public int? StackMax
        {
            get { return _StackMax; }
        }

        public override string ToString()
        {
            return DefName;
        }
    }
}
