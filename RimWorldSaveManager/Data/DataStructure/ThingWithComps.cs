﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RimWorldSaveManager.Data.DataStructure
{
    public class ThingWithComps
    {
        private XElement _xml;

        public string def
        {
            get
            {
                return _xml.Element("def").GetValue();
            }
            set
            {
                id = DataLoader.GetUniqueItemID(value + 0);
                _xml.Element("def").SetValue(value);
            }
        }

        public ThingDef defObject
        {
            get
            {
                return DataLoader.ThingDefs.FirstOrDefault(t => (t.DefName != null && t.DefName == def));
            }
        }

        public string ListName
        {
            get
            {
                if (defObject != null) return $"{defObject.Label} ({stackCount})";
                Debug.WriteLine("Warning: Unknown object def " + def);
                return $"{def} ({stackCount})";
            }
        }

        public string id
        {
            get => _xml.Element("id").GetValue();
            private set
            {
                _xml.Element("id").SetValue(value);
            }
        }

        public string pos
        {
            get
            {
                return _xml.Element("pos").GetValue();
            }
            set
            {
                _xml.Element("pos").SetValue(value);
            }
        }

        public int stackCount
        {
            get
            {
                return _xml.Element("stackCount").GetValue<int>();
            }
            set
            {
                _xml.Element("stackCount").SetValue(value);
            }
        }

        public ThingWithComps(XElement xml)
        {
            _xml = xml;
        }
    }
}
