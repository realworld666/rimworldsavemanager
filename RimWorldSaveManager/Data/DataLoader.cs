﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Globalization;
using RimWorldSaveManager.Data.DataStructure;
using System.Text;
using RimWorldSaveManager.UserControls;
using System.Text.RegularExpressions;

namespace RimWorldSaveManager
{
    public class DataLoader
    {

        private XDocument SaveDocument;
        private List<Pawn> Animals = new List<Pawn>();
        private TextInfo textInfo = CultureInfo.InvariantCulture.TextInfo;

        public static Dictionary<string, TraitDef> Traits = new Dictionary<string, TraitDef>();
        public static Dictionary<string, Hediff> Hediffs = new Dictionary<string, Hediff>();
        public static Dictionary<string, string> HumanBodyPartDescription = new Dictionary<string, string>();
        public static List<WorkType> WorkTypes = new List<WorkType>();
        public static List<string> Genders = new List<string>();
        public static Dictionary<string, Faction> Factions = new Dictionary<string, Faction>();
        public static Faction PlayerFaction;
        public static Dictionary<Faction, List<Pawn>> PawnsByFactions = new Dictionary<Faction, List<Pawn>>();
        public static Dictionary<string, Pawn> PawnsById = new Dictionary<string, Pawn>();

        public static Dictionary<string, Race> RaceDictionary = new Dictionary<string, Race>();
        public static List<PawnRelationDef> PawnRelationDefs = new List<PawnRelationDef>();
        public static Dictionary<string, ThingDef> BaseThings = new Dictionary<string, ThingDef>();     // Base classes
        public static List<ThingDef> ThingDefs = new List<ThingDef>();
        public static long CurrentGameTick;

        private Dictionary<string, List<string>> pathsForLoadingData = new Dictionary<string, List<string>>();
        public static Dictionary<string, ThingWithComps> StockpileTiles;

        public DataLoader()
        {
            Race human = new Race();
            human.DefName = "Human";
            human.Label = "Human";
            Genders.Add("Female");
            Genders.Add("Male");
            human.BodyType.Add("Male");
            human.BodyType.Add("Female");
            human.BodyType.Add("Average");
            human.BodyType.Add("Thin");
            human.BodyType.Add("Hulk");
            human.BodyType.Add("Fat");
            human.HairsByGender["Female"] = new List<Hair>();
            human.HairsByGender["Male"] = new List<Hair>();
            foreach (string firstType in new string[] { "Average", "Narrow" })
            {
                foreach (string secondType in new string[] { "Normal", "Pointy", "Wide" })
                {
                    human.HeadType.Add(new CrownType
                    {
                        CrownFirstType = firstType,
                        CrownSubType = secondType
                    });
                }
            }
            RaceDictionary[human.DefName] = human;

            ResourceLoader.Load();


            string[] allXmlFiles = Directory.GetFiles("Mods", "*.xml", SearchOption.AllDirectories);

            foreach (string xmlFile in allXmlFiles)
            {
                string[] pathComponents = xmlFile.Split('\\');
                if (!pathComponents[2].Equals("Defs"))
                {
                    continue;
                }
                string fileName = pathComponents[pathComponents.Length - 1];

                if (fileName.ToLower().Contains("race"))
                {
                    List<string> pathList;
                    if (!pathsForLoadingData.TryGetValue("race", out pathList))
                    {
                        pathList = new List<string>();
                        pathsForLoadingData["race"] = pathList;
                    }
                    pathList.Add(xmlFile);
                }
                if (fileName.ToLower().Contains("trait"))
                {
                    List<string> pathList;
                    if (!pathsForLoadingData.TryGetValue("trait", out pathList))
                    {
                        pathList = new List<string>();
                        pathsForLoadingData["trait"] = pathList;
                    }
                    pathList.Add(xmlFile);
                }
                if (fileName.ToLower().Contains("hediff"))
                {
                    List<string> pathList;
                    if (!pathsForLoadingData.TryGetValue("hediff", out pathList))
                    {
                        pathList = new List<string>();
                        pathsForLoadingData["hediff"] = pathList;
                    }
                    pathList.Add(xmlFile);
                }
                if (fileName.ToLower().Contains("backstor")) //y, ies
                {
                    List<string> pathList;
                    if (!pathsForLoadingData.TryGetValue("backstory", out pathList))
                    {
                        pathList = new List<string>();
                        pathsForLoadingData["backstory"] = pathList;
                    }
                    pathList.Add(xmlFile);
                }

                if (fileName.ToLower().Contains("worktype"))
                {
                    List<string> pathList;
                    if (!pathsForLoadingData.TryGetValue("worktype", out pathList))
                    {
                        pathList = new List<string>();
                        pathsForLoadingData["worktype"] = pathList;
                    }
                    pathList.Add(xmlFile);
                }
                if (fileName.ToLower().Contains("hair") || fileName.ToLower().Contains("antennae"))
                {
                    List<string> pathList;
                    if (!pathsForLoadingData.TryGetValue("hair", out pathList))
                    {
                        pathList = new List<string>();
                        pathsForLoadingData["hair"] = pathList;
                    }
                    pathList.Add(xmlFile);
                }

                if (fileName.ToLower().Contains("pawnrelations"))
                {
                    List<string> pathList;
                    if (!pathsForLoadingData.TryGetValue("pawnrelations", out pathList))
                    {
                        pathList = new List<string>();
                        pathsForLoadingData["pawnrelations"] = pathList;
                    }
                    pathList.Add(xmlFile);
                }

                if (xmlFile.ToLower().Contains("thingdefs"))
                {
                    List<string> pathList;
                    if (!pathsForLoadingData.TryGetValue("thingdefs", out pathList))
                    {
                        pathList = new List<string>();
                        pathsForLoadingData["thingdefs"] = pathList;
                    }
                    pathList.Add(xmlFile);
                }
            }


            List<string> filePaths;
            if (pathsForLoadingData.TryGetValue("worktype", out filePaths))
            {
                foreach (string filePath in filePaths)
                {
                    using (FileStream fileStream = File.OpenRead(filePath))
                    {
                        try
                        {
                            XElement docRoot = XDocument.Load(fileStream).Root;
                            IEnumerable<XElement> workTypeDefsRoot = docRoot.XPathSelectElements("WorkTypeDef/workTags/..");

                            if (workTypeDefsRoot.Count() == 0)
                            {
                                fileStream.Close();
                                fileStream.Dispose();
                                continue;
                            }

                            IEnumerable<WorkType> workTypeDefs = from workTypeDef in workTypeDefsRoot
                                                                 select new WorkType
                                                                 {
                                                                     DefName = workTypeDef.Element("defName").GetValue(),
                                                                     FullName = workTypeDef.Element("gerundLabel").GetValue(),
                                                                     WorkTags = workTypeDef.Element("workTags")
                                                                         .Elements("li")
                                                                         .Select(element => element.GetValue()).ToArray()
                                                                 };

                            WorkTypes.AddRange(workTypeDefs);
                        }
                        catch (Exception e)
                        {
                            // Dont care
                        }
                        fileStream.Close();
                        fileStream.Dispose();
                    }
                }
            }

            if (pathsForLoadingData.TryGetValue("race", out filePaths))
            {
                foreach (string filePath in filePaths)
                {
                    using (FileStream fileStream = File.OpenRead(filePath))
                    {
                        try
                        {
                            XElement docRoot = XDocument.Load(fileStream).Root;
                            foreach (XElement raceVars in docRoot.Descendants("AlienRace.ThingDef_AlienRace"))
                            {
                                Race race = new Race();
                                race.DefName = raceVars.Element("defName").GetValue();
                                race.Label = raceVars.Element("label").GetValue();
                                XElement alienraceElement = raceVars.Element("alienrace");
                                if (alienraceElement == null)
                                {
                                    alienraceElement = raceVars.Element("alienRace");
                                }
                                race.HairsByGender["Female"] = new List<Hair>();
                                race.HairsByGender["Male"] = new List<Hair>();
                                foreach (XElement bodyType in alienraceElement.XPathSelectElements("generalSettings/alienPartGenerator/alienbodytypes/li"))
                                {
                                    race.BodyType.Add(bodyType.GetValue());
                                }
                                foreach (XElement crownType in alienraceElement.XPathSelectElements("generalSettings/alienPartGenerator/aliencrowntypes/li"))
                                {
                                    string[] crownStrings = crownType.GetValue().Split('_');
                                    race.HeadType.Add(new CrownType
                                    {
                                        CrownFirstType = crownStrings[0],
                                        CrownSubType = crownStrings[1]
                                    });
                                }
                                if (race.HeadType.Count == 0)
                                {
                                    race.HeadType.Add(new CrownType
                                    {
                                        CrownFirstType = "Average",
                                        CrownSubType = "Normal"
                                    });
                                }
                                XElement useGenderedHeads = alienraceElement.XPathSelectElement("generalSettings/alienPartGenerator/UseGenderedHeads");
                                if (useGenderedHeads != null)
                                {
                                    race.UseGenderedHeads = Convert.ToBoolean(useGenderedHeads.GetValue());
                                }
                                foreach (XElement path in alienraceElement.XPathSelectElement("graphicPaths/li").Elements())
                                {
                                    race.GraphicPaths[path.Name.ToString().ToLower()] = path.GetValue();
                                }
                                RaceDictionary[race.DefName] = race;
                            }

                        }
                        catch (Exception e)
                        {
                            Logger.Err(e.Message);
                            // Dont care
                        }
                        fileStream.Close();
                    }
                }
            }

            if (pathsForLoadingData.TryGetValue("hair", out filePaths))
            {
                Dictionary<string, Race> tempRaceDic = RaceDictionary.Values.ToDictionary(x => x.Label.ToLower(), x => x);
                foreach (string filePath in filePaths)
                {
                    using (FileStream fileStream = File.OpenRead(filePath))
                    {
                        try
                        {
                            XElement docRoot = XDocument.Load(fileStream).Root;
                            foreach (XElement hairVars in docRoot.Descendants("HairDef"))
                            {

                                Hair hair = new Hair(hairVars.Element("hairGender").GetValue(), hairVars.Element("label").GetValue(), hairVars.Element("defName").GetValue());

                                List<Race> races = new List<Race>();
                                foreach (XElement hairTags in hairVars.XPathSelectElements("hairTags/li"))
                                {
                                    Race race;
                                    if (tempRaceDic.TryGetValue(hairTags.GetValue().ToLower(), out race))
                                    {
                                        races.Add(race);
                                    }
                                }
                                if (races.Count == 0)
                                {
                                    races.Add(RaceDictionary["Human"]);
                                }

                                foreach (Race race in races)
                                {
                                    if (hair.Gender.Equals("Any") || hair.Gender.Contains("Usually"))
                                    {
                                        foreach (List<Hair> list in race.HairsByGender.Values.ToList())
                                        {
                                            list.Add(hair);
                                        }
                                    }
                                    else
                                    {
                                        List<Hair> hairListForGender;
                                        if (race.HairsByGender.TryGetValue(hair.Gender, out hairListForGender))
                                        {
                                            hairListForGender.Add(hair);
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Logger.Err(e.Message);
                        }
                        fileStream.Close();
                    }
                }
                foreach (Race race in RaceDictionary.Values.ToList())
                {
                    foreach (List<Hair> list in race.HairsByGender.Values.ToList())
                    {
                        list.Sort(delegate (Hair x, Hair y)
                        {
                            return x.Def.CompareTo(y.Def);
                        });
                    }
                }
            }

            if (pathsForLoadingData.TryGetValue("trait", out filePaths))
            {
                foreach (string filePath in filePaths)
                {
                    using (FileStream fileStream = File.OpenRead(filePath))
                    {
                        try
                        {
                            XElement docRoot = XDocument.Load(fileStream).Root;
                            foreach (XElement traitDef in docRoot.Descendants("TraitDef"))
                            {
                                IEnumerable<TraitDef> traits = (from trait in traitDef.XPathSelectElements("degreeDatas/li")
                                                                select new TraitDef
                                                                {
                                                                    Def = traitDef.Element("defName").Value,
                                                                    Label = textInfo.ToTitleCase(trait.Element("label").Value),
                                                                    Degree = trait.Element("degree") != null ? trait.Element("degree").Value : "0"
                                                                });

                                foreach (TraitDef trait in traits)
                                    if (!Traits.ContainsKey(trait.Def + trait.Degree))
                                        Traits.Add(trait.Def + trait.Degree, trait);
                            }
                        }
                        catch (Exception e)
                        {
                            // Dont care
                        }
                        fileStream.Close();

                    }
                }
            }

            if (pathsForLoadingData.TryGetValue("hediff", out filePaths))
            {
                foreach (string filePath in filePaths)
                {
                    using (FileStream fileStream = File.OpenRead(filePath))
                    {
                        try
                        {
                            XElement docRoot = XDocument.Load(fileStream).Root;

                            IEnumerable<XElement> hediffRoots = docRoot.XPathSelectElements("HediffDef/hediffClass/..");

                            if (hediffRoots.Count() == 0)
                            {
                                fileStream.Close();
                                fileStream.Dispose();
                                continue;
                            }

                            foreach (XElement hediffRoot in hediffRoots)
                            {

                                string parentClass = hediffRoot.Element("hediffClass").Value;
                                string parentName = hediffRoot.Attribute("Name") != null ? hediffRoot.Attribute("Name").Value : "None";

                                Hediff coreHediff;

                                if (!Hediffs.TryGetValue(parentClass, out coreHediff))
                                    Hediffs.Add(parentClass, coreHediff = new Hediff(parentClass, parentName));

                                IEnumerable<HediffDef> hediffs = (from hediff in docRoot.XPathSelectElements("//HediffDef[boolean(@ParentName) and not(@Abstract)]")
                                               .Where(x => x.Attribute("ParentName").Value == parentName)
                                                                  select new HediffDef
                                                                  {
                                                                      ParentClass = parentClass,
                                                                      ParentName = hediff.Attribute("ParentName").Value,
                                                                      Def = hediff.Element("defName").Value,
                                                                      Label = textInfo.ToTitleCase(hediff.Element("label").Value),
                                                                  });

                                foreach (HediffDef hediff in hediffs)
                                    coreHediff.SubDiffs[hediff.Def] = hediff;
                            }
                        }
                        catch (Exception e)
                        {
                            // Dont care
                        }
                        fileStream.Close();
                        fileStream.Dispose();

                    }
                }
            }

            if (pathsForLoadingData.TryGetValue("backstory", out filePaths))
            {
                foreach (string filePath in filePaths)
                {
                    using (FileStream fileStream = File.OpenRead(filePath))
                    {
                        try
                        {
                            XElement docRoot = XDocument.Load(fileStream).Root;
                            foreach (XElement def in docRoot.Descendants("AlienRace.BackstoryDef"))
                            {
                                Backstory backstory = new Backstory
                                {
                                    Id = (string)def.Element("defName"),
                                    Title = (string)def.Element("title"),
                                    DisplayTitle = "(AlienRace)" + (string)def.Element("title"),
                                    TitleShort = (string)def.Element("titleShort"),
                                    Description = (string)def.Element("baseDescription"),
                                    Slot = (string)def.Element("slot"),
                                    SkillGains = new Dictionary<string, int>(),
                                    WorkDisables = new List<string>()
                                };
                                foreach (XElement skillGain in def.XPathSelectElements("skillGains/li"))
                                {
                                    string defName = (string)skillGain.Element("defName");
                                    int amount = Convert.ToInt32(skillGain.Element("amount").GetValue());
                                    backstory.SkillGains.Add(defName, amount);
                                }
                                foreach (XElement workDisables in def.XPathSelectElements("workDisables/li"))
                                {
                                    backstory.WorkDisables.Add(workDisables.GetValue());
                                }
                                ResourceLoader.Backstories[backstory.Id] = backstory;

                                if (string.IsNullOrEmpty(backstory.Slot))
                                {
                                    ResourceLoader.ChildhoodStories.Add(backstory);
                                    ResourceLoader.AdulthoodStories.Add(backstory);
                                }
                                else if (backstory.Slot == "Childhood")
                                {
                                    ResourceLoader.ChildhoodStories.Add(backstory);
                                }
                                else
                                {
                                    ResourceLoader.AdulthoodStories.Add(backstory);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Logger.Err(e.Message);
                            // Dont care
                        }
                        fileStream.Close();
                        fileStream.Dispose();
                    }
                }
            }

            if (pathsForLoadingData.TryGetValue("pawnrelations", out filePaths))
            {
                foreach (string filePath in filePaths)
                {
                    using (FileStream fileStream = File.OpenRead(filePath))
                    {
                        try
                        {
                            XElement docRoot = XDocument.Load(fileStream).Root;
                            foreach (XElement relationDef in docRoot.XPathSelectElements("PawnRelationDef"))
                            {
                                PawnRelationDefs.Add(new PawnRelationDef(relationDef));
                            }
                        }
                        catch (Exception e)
                        {
                            Logger.Err(e.Message);
                            // Dont care
                        }
                        fileStream.Close();
                        fileStream.Dispose();
                    }
                }
            }

            if (pathsForLoadingData.TryGetValue("thingdefs", out filePaths))
            {
                // Two passes. First get all abstract classes
                foreach (string filePath in filePaths)
                {
                    using (FileStream fileStream = File.OpenRead(filePath))
                    {
                        try
                        {
                            XElement docRoot = XDocument.Load(fileStream).Root;
                            foreach (XElement thingDefs in docRoot.XPathSelectElements("ThingDef"))
                            {
                                if (thingDefs.Attribute("Abstract")?.GetValue().Equals("True") == true)
                                {
                                    BaseThings.Add(thingDefs.Attribute("Name").GetValue(), new ThingDef(thingDefs));
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Logger.Err(e.Message);
                            // Dont care
                        }
                        fileStream.Close();
                        fileStream.Dispose();
                    }
                }
                // Then get all the actual thing defs
                foreach (string filePath in filePaths)
                {
                    using (FileStream fileStream = File.OpenRead(filePath))
                    {
                        try
                        {
                            XElement docRoot = XDocument.Load(fileStream).Root;
                            foreach (XElement thingDefs in docRoot.XPathSelectElements("ThingDef"))
                            {
                                var isAbstract = thingDefs.Attribute("Abstract");
                                if (isAbstract == null || isAbstract.GetValue().Equals("False"))
                                {
                                    ThingDefs.Add(new ThingDef(thingDefs));
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Logger.Err(e.Message);
                            // Dont care
                        }
                        fileStream.Close();
                        fileStream.Dispose();
                    }
                }
            }


            PawnRelationDefs = PawnRelationDefs.OrderBy(x => x.DefName).ToList();
            ResourceLoader.ChildhoodStories = ResourceLoader.ChildhoodStories.OrderBy(x => x.DisplayTitle).ToList();
            ResourceLoader.AdulthoodStories = ResourceLoader.AdulthoodStories.OrderBy(x => x.DisplayTitle).ToList();

        }

        public static string GetUniquePawnID(string startID)
        {
            Regex rgx = new Regex(@"\d+$");
            int startIndex = int.Parse(rgx.Match(startID).Value);
            string prefix = rgx.Replace(startID, "");

            bool found = false;
            while (true)
            {
                foreach (KeyValuePair<Faction, List<Pawn>> factionPawns in PawnsByFactions)
                {
                    if (factionPawns.Value.Any(p => p.Id.Equals(string.Format("{0}{1}", prefix, startIndex))))
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    break;
                }

                startIndex++;
                found = false;
            }

            return string.Format("{0}{1}", prefix, startIndex);
        }

        public static string GetUniqueItemID(string startID)
        {
            Regex rgx = new Regex(@"\d+$");
            int startIndex = int.Parse(rgx.Match(startID).Value);
            string prefix = rgx.Replace(startID, "");

            bool found = false;
            while (true)
            {
                foreach (KeyValuePair<string, ThingWithComps> tile in StockpileTiles)
                {
                    if (tile.Value != null && tile.Value.id.Equals(string.Format("{0}{1}", prefix, startIndex)))
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    break;
                }

                startIndex++;
                found = false;
            }

            return string.Format("{0}{1}", prefix, startIndex);
        }



        public bool LoadData(string path, TabControl tabControl)
        {
            tabControl.TabPages.Clear();
            Animals.Clear();

            //try {
            SaveDocument = XDocument.Load(path);

            CurrentGameTick = long.Parse(SaveDocument.Root.XPathSelectElement("game/tickManager/ticksGame").GetValue());

            string playerFactionDef = EvaluateSingle<XElement>("scenario/playerFaction/factionDef").Value;

            foreach (XElement element in SaveDocument.Root.XPathSelectElements("game/world/factionManager/allFactions/li"))
            {
                Faction faction = new Faction(element);
                if (faction.Def.Equals(playerFactionDef))
                {
                    PlayerFaction = faction;
                }
                Factions[faction.FactionIDString] = faction;
                PawnsByFactions[faction] = new List<Pawn>();
            }

            //Console.WriteLine($"playerFaction:{playerFaction}, colonyFaction:{colonyFaction}");

            Dictionary<String, List<PawnData>> pawnDataDir = new Dictionary<string, List<PawnData>>();
            foreach (XElement pawnData in SaveDocument.Descendants("pawn"))
            {
                String key = pawnData.GetValue();

                List<PawnData> pawnDataList;
                if (!pawnDataDir.TryGetValue(key, out pawnDataList))
                {
                    pawnDataList = new List<PawnData>();
                    pawnDataDir[key] = pawnDataList;
                }

                pawnDataList.Add(new PawnData(pawnData.Parent));
            }

            foreach (XElement pawn in SaveDocument.Root.XPathSelectElements("game/world/worldPawns/pawnsAlive/li"))
            {
                Pawn p = new Pawn(pawn);
                if (p.Faction != null)
                {
                    List<PawnData> pawnDataList;
                    if (!pawnDataDir.TryGetValue(p.PawnId, out pawnDataList))
                    {
                        pawnDataList = new List<PawnData>();
                    }
                    p.addPawnData(pawnDataList);

                    PawnsById[p.PawnId] = p;
                    Faction faction = Factions[p.Faction];
                    PawnsByFactions[faction].Add(p);
                }

            }

            // Find all stockpiles
            StockpileTiles = new Dictionary<string, ThingWithComps>();
            // For each map
            foreach (XElement mapElement in SaveDocument.Root.XPathSelectElements("game/maps/li"))
            {
                // Get each "zone"
                IEnumerable<XElement> zones = mapElement.XPathSelectElements("zoneManager/allZones/li").Where(e => e.Attribute("Class").GetValue().Equals("Zone_Stockpile"));
                foreach (XElement zone in zones)
                {
                    // Add all cells
                    IEnumerable<string> cells = zone.XPathSelectElements("cells/li").Select(e => e.GetValue<string>());
                    foreach (string cell in cells)
                    {
                        StockpileTiles.Add(cell, null);
                    }
                }
            }

            // Find all items in stockpiles
            // For each map
            foreach (XElement mapElement in SaveDocument.Root.XPathSelectElements("game/maps/li"))
            {
                // Get things
                IEnumerable<XElement> things = mapElement.XPathSelectElements("things/thing").Where(e => e.Attribute("Class").GetValue().Equals("ThingWithComps"));
                foreach (XElement thing in things)
                {
                    ThingWithComps thingInst = new ThingWithComps(thing);
                    if (StockpileTiles.ContainsKey(thingInst.pos))
                    {
                        StockpileTiles[thingInst.pos] = thingInst;
                    }
                }
            }

            foreach (XElement pawn in SaveDocument.Descendants("thing"))
            {
                if ((string)pawn.Attribute("Class") == "Pawn")
                {


                    Pawn p = new Pawn(pawn);
                    if (p.Faction != null)
                    {
                        List<PawnData> pawnDataList;
                        if (!pawnDataDir.TryGetValue(p.PawnId, out pawnDataList))
                        {
                            pawnDataList = new List<PawnData>();
                        }
                        p.addPawnData(pawnDataList);

                        PawnsById[p.PawnId] = p;
                        Faction faction = Factions[p.Faction];
                        PawnsByFactions[faction].Add(p);
                    }

                }
            }

            if (PawnsByFactions[PlayerFaction].Count == 0)
            {
                throw new Exception("No characters found!\nTry playing the game a little more.");
            }

            var colonistPage = new ColonistPage(PawnsByFactions[PlayerFaction].Where(p => p.Race != null).ToList());
            colonistPage.Dock = DockStyle.Fill;
            var animalPage = new AnimalPage(PawnsByFactions[PlayerFaction].Where(p => p.Race == null).ToList());
            animalPage.Dock = DockStyle.Fill;

            TabPage colonisTabPage = new TabPage("Colonists");
            TabPage animalsTabPage = new TabPage("Animals");
            TabPage relationsTabPage = new TabPage("Relations");
            TabPage stockpileTabPage = new TabPage("Stockpile");
            colonisTabPage.Controls.Add(colonistPage);
            animalsTabPage.Controls.Add(animalPage);
            relationsTabPage.Controls.Add(new RelationPage());
            stockpileTabPage.Controls.Add(new InventoryPage());


            tabControl.TabPages.Add(colonisTabPage);
            tabControl.TabPages.Add(animalsTabPage);
            tabControl.TabPages.Add(relationsTabPage);
            tabControl.TabPages.Add(stockpileTabPage);


            return true;
        }

        public bool SaveData(string path)
        {
            SaveDocument.Save(path);

            MessageBox.Show("Successfully saved changes!");
            return true;
        }

        private IEnumerable<T> EvaluateList<T>(string eval)
        {
            return (SaveDocument.Root.Element("game").XPathEvaluate(eval) as IEnumerable).Cast<T>();
        }

        private T EvaluateSingle<T>(string eval)
        {
            return (SaveDocument.Root.Element("game").XPathEvaluate(eval) as IEnumerable).Cast<T>().First();
        }

        private T Evaluate<T>(string eval)
        {
            return (T)SaveDocument.Root.Element("game").XPathEvaluate(eval);
        }

        private IEnumerable<T> EvaluateList<T>(XNode node, string eval)
        {
            return (node.XPathEvaluate(eval) as IEnumerable).Cast<T>();
        }

        private T EvaluateSingle<T>(XNode node, string eval)
        {
            return (node.XPathEvaluate(eval) as IEnumerable).Cast<T>().First();
        }

        private T Evaluate<T>(XNode node, string eval)
        {
            return (T)node.XPathEvaluate(eval);
        }


    }
}
