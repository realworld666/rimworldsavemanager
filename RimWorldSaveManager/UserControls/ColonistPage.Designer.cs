﻿namespace RimWorldSaveManager.UserControls
{
    partial class ColonistPage
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.skillsGroupBox = new System.Windows.Forms.GroupBox();
            this.ageGroupBox = new System.Windows.Forms.GroupBox();
            this.chronoAgeField = new System.Windows.Forms.NumericUpDown();
            this.bioAgeField = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.adulthoodComboBox = new System.Windows.Forms.ComboBox();
            this.childhoodComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.traitComboBox = new System.Windows.Forms.ComboBox();
            this.btnRemoveTrait = new System.Windows.Forms.Button();
            this.btnAddTrait = new System.Windows.Forms.Button();
            this.listBoxTraits = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnRemoveInjury = new System.Windows.Forms.Button();
            this.listBoxInjuries = new System.Windows.Forms.ListBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.DescriptionText = new System.Windows.Forms.TextBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.nameGroupBox = new System.Windows.Forms.GroupBox();
            this.textBoxLastname = new System.Windows.Forms.TextBox();
            this.textBoxNickname = new System.Windows.Forms.TextBox();
            this.textBoxFirstname = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelDefinition = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.numericUpDownMelanin = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.panelHairColor = new System.Windows.Forms.Panel();
            this.buttonHairColor = new System.Windows.Forms.Button();
            this.comboBoxHairDef = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBoxHeadType = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBoxBodyType = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxGender = new System.Windows.Forms.ComboBox();
            this.colorDialogHair = new System.Windows.Forms.ColorDialog();
            this.newColonist = new System.Windows.Forms.Button();
            this.needsGroupBox = new System.Windows.Forms.GroupBox();
            this.numericUpDownMood = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.numericUpDownFood = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.numericUpDownRest = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.ageGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chronoAgeField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bioAgeField)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.nameGroupBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMelanin)).BeginInit();
            this.needsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMood)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFood)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRest)).BeginInit();
            this.SuspendLayout();
            // 
            // skillsGroupBox
            // 
            this.skillsGroupBox.Location = new System.Drawing.Point(187, 66);
            this.skillsGroupBox.Name = "skillsGroupBox";
            this.skillsGroupBox.Size = new System.Drawing.Size(313, 336);
            this.skillsGroupBox.TabIndex = 1;
            this.skillsGroupBox.TabStop = false;
            this.skillsGroupBox.Text = "Skills";
            // 
            // ageGroupBox
            // 
            this.ageGroupBox.Controls.Add(this.chronoAgeField);
            this.ageGroupBox.Controls.Add(this.bioAgeField);
            this.ageGroupBox.Controls.Add(this.label2);
            this.ageGroupBox.Controls.Add(this.label1);
            this.ageGroupBox.Location = new System.Drawing.Point(731, 6);
            this.ageGroupBox.Name = "ageGroupBox";
            this.ageGroupBox.Size = new System.Drawing.Size(164, 100);
            this.ageGroupBox.TabIndex = 2;
            this.ageGroupBox.TabStop = false;
            this.ageGroupBox.Text = "Age";
            // 
            // chronoAgeField
            // 
            this.chronoAgeField.DecimalPlaces = 2;
            this.chronoAgeField.Location = new System.Drawing.Point(83, 42);
            this.chronoAgeField.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.chronoAgeField.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.chronoAgeField.Name = "chronoAgeField";
            this.chronoAgeField.Size = new System.Drawing.Size(65, 20);
            this.chronoAgeField.TabIndex = 3;
            this.chronoAgeField.ValueChanged += new System.EventHandler(this.chronoAgeField_ValueChanged);
            // 
            // bioAgeField
            // 
            this.bioAgeField.DecimalPlaces = 2;
            this.bioAgeField.Location = new System.Drawing.Point(83, 16);
            this.bioAgeField.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.bioAgeField.Name = "bioAgeField";
            this.bioAgeField.Size = new System.Drawing.Size(66, 20);
            this.bioAgeField.TabIndex = 2;
            this.bioAgeField.ValueChanged += new System.EventHandler(this.bioAgeField_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Chronological";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Biological";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.adulthoodComboBox);
            this.groupBox4.Controls.Add(this.childhoodComboBox);
            this.groupBox4.Location = new System.Drawing.Point(506, 263);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(389, 91);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Backstory";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Adulthood";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Childhood";
            // 
            // adulthoodComboBox
            // 
            this.adulthoodComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.adulthoodComboBox.DropDownHeight = 400;
            this.adulthoodComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.adulthoodComboBox.DropDownWidth = 200;
            this.adulthoodComboBox.FormattingEnabled = true;
            this.adulthoodComboBox.IntegralHeight = false;
            this.adulthoodComboBox.Location = new System.Drawing.Point(83, 46);
            this.adulthoodComboBox.Name = "adulthoodComboBox";
            this.adulthoodComboBox.Size = new System.Drawing.Size(296, 21);
            this.adulthoodComboBox.TabIndex = 1;
            this.adulthoodComboBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.Backstory_DrawItem);
            this.adulthoodComboBox.SelectedIndexChanged += new System.EventHandler(this.backstoryComboBox_SelectedIndexChanged);
            this.adulthoodComboBox.DropDownClosed += new System.EventHandler(this.DropDownClosed);
            // 
            // childhoodComboBox
            // 
            this.childhoodComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.childhoodComboBox.DropDownHeight = 400;
            this.childhoodComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.childhoodComboBox.DropDownWidth = 200;
            this.childhoodComboBox.FormattingEnabled = true;
            this.childhoodComboBox.IntegralHeight = false;
            this.childhoodComboBox.Location = new System.Drawing.Point(83, 19);
            this.childhoodComboBox.Name = "childhoodComboBox";
            this.childhoodComboBox.Size = new System.Drawing.Size(296, 21);
            this.childhoodComboBox.TabIndex = 0;
            this.childhoodComboBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.Backstory_DrawItem);
            this.childhoodComboBox.SelectedIndexChanged += new System.EventHandler(this.backstoryComboBox_SelectedIndexChanged);
            this.childhoodComboBox.DropDownClosed += new System.EventHandler(this.DropDownClosed);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.traitComboBox);
            this.groupBox2.Controls.Add(this.btnRemoveTrait);
            this.groupBox2.Controls.Add(this.btnAddTrait);
            this.groupBox2.Controls.Add(this.listBoxTraits);
            this.groupBox2.Location = new System.Drawing.Point(902, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(353, 188);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Traits";
            // 
            // traitComboBox
            // 
            this.traitComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.traitComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.traitComboBox.FormattingEnabled = true;
            this.traitComboBox.Location = new System.Drawing.Point(7, 158);
            this.traitComboBox.Name = "traitComboBox";
            this.traitComboBox.Size = new System.Drawing.Size(208, 21);
            this.traitComboBox.TabIndex = 3;
            // 
            // btnRemoveTrait
            // 
            this.btnRemoveTrait.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemoveTrait.Location = new System.Drawing.Point(287, 157);
            this.btnRemoveTrait.Name = "btnRemoveTrait";
            this.btnRemoveTrait.Size = new System.Drawing.Size(60, 23);
            this.btnRemoveTrait.TabIndex = 2;
            this.btnRemoveTrait.Text = "Remove";
            this.btnRemoveTrait.UseVisualStyleBackColor = true;
            this.btnRemoveTrait.Click += new System.EventHandler(this.btnRemoveTrait_Click);
            // 
            // btnAddTrait
            // 
            this.btnAddTrait.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddTrait.Location = new System.Drawing.Point(221, 157);
            this.btnAddTrait.Name = "btnAddTrait";
            this.btnAddTrait.Size = new System.Drawing.Size(60, 23);
            this.btnAddTrait.TabIndex = 1;
            this.btnAddTrait.Text = "Add";
            this.btnAddTrait.UseVisualStyleBackColor = true;
            this.btnAddTrait.Click += new System.EventHandler(this.btnAddTrait_Click);
            // 
            // listBoxTraits
            // 
            this.listBoxTraits.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxTraits.FormattingEnabled = true;
            this.listBoxTraits.Location = new System.Drawing.Point(6, 14);
            this.listBoxTraits.Name = "listBoxTraits";
            this.listBoxTraits.Size = new System.Drawing.Size(347, 134);
            this.listBoxTraits.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox3.Controls.Add(this.btnRemoveInjury);
            this.groupBox3.Controls.Add(this.listBoxInjuries);
            this.groupBox3.Location = new System.Drawing.Point(506, 360);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(389, 430);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Injuries";
            // 
            // btnRemoveInjury
            // 
            this.btnRemoveInjury.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemoveInjury.Location = new System.Drawing.Point(323, 399);
            this.btnRemoveInjury.Name = "btnRemoveInjury";
            this.btnRemoveInjury.Size = new System.Drawing.Size(60, 23);
            this.btnRemoveInjury.TabIndex = 2;
            this.btnRemoveInjury.Text = "Remove";
            this.btnRemoveInjury.UseVisualStyleBackColor = true;
            this.btnRemoveInjury.Click += new System.EventHandler(this.btnRemoveInjury_Click);
            // 
            // listBoxInjuries
            // 
            this.listBoxInjuries.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBoxInjuries.FormattingEnabled = true;
            this.listBoxInjuries.Location = new System.Drawing.Point(7, 20);
            this.listBoxInjuries.Name = "listBoxInjuries";
            this.listBoxInjuries.Size = new System.Drawing.Size(372, 368);
            this.listBoxInjuries.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.DescriptionText);
            this.groupBox5.Location = new System.Drawing.Point(902, 194);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox5.Size = new System.Drawing.Size(361, 596);
            this.groupBox5.TabIndex = 13;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Description";
            // 
            // DescriptionText
            // 
            this.DescriptionText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DescriptionText.BackColor = System.Drawing.SystemColors.Window;
            this.DescriptionText.Location = new System.Drawing.Point(5, 18);
            this.DescriptionText.Multiline = true;
            this.DescriptionText.Name = "DescriptionText";
            this.DescriptionText.ReadOnly = true;
            this.DescriptionText.Size = new System.Drawing.Size(351, 570);
            this.DescriptionText.TabIndex = 0;
            // 
            // listBox1
            // 
            this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(0, 0);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(181, 758);
            this.listBox1.TabIndex = 14;
            this.listBox1.Click += new System.EventHandler(this.listBox1_Click);
            // 
            // nameGroupBox
            // 
            this.nameGroupBox.Controls.Add(this.textBoxLastname);
            this.nameGroupBox.Controls.Add(this.textBoxNickname);
            this.nameGroupBox.Controls.Add(this.textBoxFirstname);
            this.nameGroupBox.Controls.Add(this.label5);
            this.nameGroupBox.Controls.Add(this.label4);
            this.nameGroupBox.Controls.Add(this.label3);
            this.nameGroupBox.Location = new System.Drawing.Point(506, 6);
            this.nameGroupBox.Name = "nameGroupBox";
            this.nameGroupBox.Size = new System.Drawing.Size(219, 100);
            this.nameGroupBox.TabIndex = 15;
            this.nameGroupBox.TabStop = false;
            this.nameGroupBox.Text = "Name";
            // 
            // textBoxLastname
            // 
            this.textBoxLastname.Location = new System.Drawing.Point(65, 70);
            this.textBoxLastname.Name = "textBoxLastname";
            this.textBoxLastname.Size = new System.Drawing.Size(141, 20);
            this.textBoxLastname.TabIndex = 5;
            this.textBoxLastname.TextChanged += new System.EventHandler(this.textBoxLastname_TextChanged);
            // 
            // textBoxNickname
            // 
            this.textBoxNickname.Location = new System.Drawing.Point(65, 44);
            this.textBoxNickname.Name = "textBoxNickname";
            this.textBoxNickname.Size = new System.Drawing.Size(141, 20);
            this.textBoxNickname.TabIndex = 4;
            this.textBoxNickname.TextChanged += new System.EventHandler(this.textBoxNickname_TextChanged);
            // 
            // textBoxFirstname
            // 
            this.textBoxFirstname.Location = new System.Drawing.Point(65, 18);
            this.textBoxFirstname.Name = "textBoxFirstname";
            this.textBoxFirstname.Size = new System.Drawing.Size(141, 20);
            this.textBoxFirstname.TabIndex = 3;
            this.textBoxFirstname.TextChanged += new System.EventHandler(this.textBoxFirstname_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Nickname";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Lastname";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Firstname";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelDefinition);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Location = new System.Drawing.Point(187, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(313, 54);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Infos";
            // 
            // labelDefinition
            // 
            this.labelDefinition.AutoSize = true;
            this.labelDefinition.Location = new System.Drawing.Point(68, 21);
            this.labelDefinition.Name = "labelDefinition";
            this.labelDefinition.Size = new System.Drawing.Size(35, 13);
            this.labelDefinition.TabIndex = 1;
            this.labelDefinition.Text = "label8";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Definition:";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.numericUpDownMelanin);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this.panelHairColor);
            this.groupBox6.Controls.Add(this.buttonHairColor);
            this.groupBox6.Controls.Add(this.comboBoxHairDef);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.comboBoxHeadType);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.comboBoxBodyType);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Controls.Add(this.comboBoxGender);
            this.groupBox6.Location = new System.Drawing.Point(506, 110);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(389, 147);
            this.groupBox6.TabIndex = 17;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Gender/Apperance";
            // 
            // numericUpDownMelanin
            // 
            this.numericUpDownMelanin.DecimalPlaces = 8;
            this.numericUpDownMelanin.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownMelanin.Location = new System.Drawing.Point(248, 53);
            this.numericUpDownMelanin.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMelanin.Name = "numericUpDownMelanin";
            this.numericUpDownMelanin.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownMelanin.TabIndex = 13;
            this.numericUpDownMelanin.ValueChanged += new System.EventHandler(this.numericUpDownMelanin_ValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(198, 55);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Melanin";
            // 
            // panelHairColor
            // 
            this.panelHairColor.Location = new System.Drawing.Point(282, 114);
            this.panelHairColor.Name = "panelHairColor";
            this.panelHairColor.Size = new System.Drawing.Size(40, 23);
            this.panelHairColor.TabIndex = 11;
            // 
            // buttonHairColor
            // 
            this.buttonHairColor.Location = new System.Drawing.Point(201, 114);
            this.buttonHairColor.Name = "buttonHairColor";
            this.buttonHairColor.Size = new System.Drawing.Size(75, 23);
            this.buttonHairColor.TabIndex = 9;
            this.buttonHairColor.Text = "Hair-Color";
            this.buttonHairColor.UseVisualStyleBackColor = true;
            this.buttonHairColor.Click += new System.EventHandler(this.buttonHairColor_Click);
            // 
            // comboBoxHairDef
            // 
            this.comboBoxHairDef.FormattingEnabled = true;
            this.comboBoxHairDef.Location = new System.Drawing.Point(65, 114);
            this.comboBoxHairDef.Name = "comboBoxHairDef";
            this.comboBoxHairDef.Size = new System.Drawing.Size(121, 21);
            this.comboBoxHairDef.TabIndex = 8;
            this.comboBoxHairDef.SelectedIndexChanged += new System.EventHandler(this.comboBoxHairDef_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 117);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 13);
            this.label12.TabIndex = 7;
            this.label12.Text = "HairType";
            // 
            // comboBoxHeadType
            // 
            this.comboBoxHeadType.FormattingEnabled = true;
            this.comboBoxHeadType.Location = new System.Drawing.Point(65, 84);
            this.comboBoxHeadType.Name = "comboBoxHeadType";
            this.comboBoxHeadType.Size = new System.Drawing.Size(303, 21);
            this.comboBoxHeadType.TabIndex = 5;
            this.comboBoxHeadType.SelectedIndexChanged += new System.EventHandler(this.comboBoxHeadType_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 84);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "HeadType";
            // 
            // comboBoxBodyType
            // 
            this.comboBoxBodyType.FormattingEnabled = true;
            this.comboBoxBodyType.Location = new System.Drawing.Point(65, 51);
            this.comboBoxBodyType.Name = "comboBoxBodyType";
            this.comboBoxBodyType.Size = new System.Drawing.Size(121, 21);
            this.comboBoxBodyType.TabIndex = 3;
            this.comboBoxBodyType.SelectedIndexChanged += new System.EventHandler(this.comboBoxBodyType_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 55);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "BodyType";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Gender";
            // 
            // comboBoxGender
            // 
            this.comboBoxGender.FormattingEnabled = true;
            this.comboBoxGender.Location = new System.Drawing.Point(65, 20);
            this.comboBoxGender.Name = "comboBoxGender";
            this.comboBoxGender.Size = new System.Drawing.Size(121, 21);
            this.comboBoxGender.TabIndex = 0;
            this.comboBoxGender.SelectedIndexChanged += new System.EventHandler(this.comboBoxGender_SelectedIndexChanged);
            // 
            // newColonist
            // 
            this.newColonist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.newColonist.Location = new System.Drawing.Point(121, 764);
            this.newColonist.Name = "newColonist";
            this.newColonist.Size = new System.Drawing.Size(60, 23);
            this.newColonist.TabIndex = 3;
            this.newColonist.Text = "Add New";
            this.newColonist.UseVisualStyleBackColor = true;
            this.newColonist.Click += new System.EventHandler(this.newColonist_Click);
            // 
            // needsGroupBox
            // 
            this.needsGroupBox.Controls.Add(this.numericUpDownRest);
            this.needsGroupBox.Controls.Add(this.label16);
            this.needsGroupBox.Controls.Add(this.numericUpDownFood);
            this.needsGroupBox.Controls.Add(this.label15);
            this.needsGroupBox.Controls.Add(this.numericUpDownMood);
            this.needsGroupBox.Controls.Add(this.label14);
            this.needsGroupBox.Location = new System.Drawing.Point(187, 408);
            this.needsGroupBox.Name = "needsGroupBox";
            this.needsGroupBox.Size = new System.Drawing.Size(313, 93);
            this.needsGroupBox.TabIndex = 2;
            this.needsGroupBox.TabStop = false;
            this.needsGroupBox.Text = "Needs";
            // 
            // numericUpDownMood
            // 
            this.numericUpDownMood.DecimalPlaces = 2;
            this.numericUpDownMood.Location = new System.Drawing.Point(83, 14);
            this.numericUpDownMood.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownMood.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDownMood.Name = "numericUpDownMood";
            this.numericUpDownMood.Size = new System.Drawing.Size(65, 20);
            this.numericUpDownMood.TabIndex = 5;
            this.numericUpDownMood.ValueChanged += new System.EventHandler(this.numericUpDownMood_ValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 13);
            this.label14.TabIndex = 4;
            this.label14.Text = "Mood";
            // 
            // numericUpDownFood
            // 
            this.numericUpDownFood.DecimalPlaces = 2;
            this.numericUpDownFood.Location = new System.Drawing.Point(83, 39);
            this.numericUpDownFood.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownFood.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDownFood.Name = "numericUpDownFood";
            this.numericUpDownFood.Size = new System.Drawing.Size(65, 20);
            this.numericUpDownFood.TabIndex = 7;
            this.numericUpDownFood.ValueChanged += new System.EventHandler(this.numericUpDownFood_ValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 41);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(31, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "Food";
            // 
            // numericUpDownRest
            // 
            this.numericUpDownRest.DecimalPlaces = 2;
            this.numericUpDownRest.Location = new System.Drawing.Point(83, 63);
            this.numericUpDownRest.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownRest.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numericUpDownRest.Name = "numericUpDownRest";
            this.numericUpDownRest.Size = new System.Drawing.Size(65, 20);
            this.numericUpDownRest.TabIndex = 9;
            this.numericUpDownRest.ValueChanged += new System.EventHandler(this.numericUpDownRest_ValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 65);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "Rest";
            // 
            // ColonistPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.needsGroupBox);
            this.Controls.Add(this.newColonist);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.nameGroupBox);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.ageGroupBox);
            this.Controls.Add(this.skillsGroupBox);
            this.Name = "ColonistPage";
            this.Size = new System.Drawing.Size(1266, 793);
            this.ageGroupBox.ResumeLayout(false);
            this.ageGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chronoAgeField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bioAgeField)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.nameGroupBox.ResumeLayout(false);
            this.nameGroupBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMelanin)).EndInit();
            this.needsGroupBox.ResumeLayout(false);
            this.needsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMood)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFood)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRest)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox skillsGroupBox;
        private System.Windows.Forms.GroupBox ageGroupBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox adulthoodComboBox;
        private System.Windows.Forms.ComboBox childhoodComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox traitComboBox;
        private System.Windows.Forms.Button btnRemoveTrait;
        private System.Windows.Forms.Button btnAddTrait;
        public System.Windows.Forms.ListBox listBoxTraits;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnRemoveInjury;
        public System.Windows.Forms.ListBox listBoxInjuries;
        private System.Windows.Forms.NumericUpDown chronoAgeField;
        private System.Windows.Forms.NumericUpDown bioAgeField;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox DescriptionText;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.GroupBox nameGroupBox;
        private System.Windows.Forms.TextBox textBoxLastname;
        private System.Windows.Forms.TextBox textBoxNickname;
        private System.Windows.Forms.TextBox textBoxFirstname;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelDefinition;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox comboBoxHairDef;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBoxHeadType;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBoxBodyType;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBoxGender;
        private System.Windows.Forms.ColorDialog colorDialogHair;
        private System.Windows.Forms.Button buttonHairColor;
        private System.Windows.Forms.Panel panelHairColor;
        private System.Windows.Forms.NumericUpDown numericUpDownMelanin;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button newColonist;
        private System.Windows.Forms.GroupBox needsGroupBox;
        private System.Windows.Forms.NumericUpDown numericUpDownMood;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown numericUpDownRest;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown numericUpDownFood;
        private System.Windows.Forms.Label label15;
    }
}
