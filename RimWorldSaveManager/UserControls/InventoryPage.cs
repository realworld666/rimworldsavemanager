﻿using RimWorldSaveManager.Data.DataStructure;
using System;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace RimWorldSaveManager.UserControls
{
    public partial class InventoryPage : UserControl
    {
        private BindingList<Pawn> _pawnBindingList;

        public InventoryPage()
        {
            InitializeComponent();

            var thingArray = DataLoader.StockpileTiles.Select(row =>
            {
                string name = "Empty";
                if (row.Value != null)
                {
                    name = row.Value.ListName;
                }
                return new
                {
                    Item = row.Value,
                    Name = name
                };
            });

            itemCombo.DataSource = DataLoader.ThingDefs.OrderBy(t => t.Label).ToList();
            itemCombo.DisplayMember = "Label";

            stockpileList.DataSource = thingArray.ToList();
            stockpileList.DisplayMember = "Name";

            UpdateForm(thingArray.FirstOrDefault()?.Item);
        }

        private void stockpileList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ThingWithComps selected = GetSelected();
            UpdateForm(selected);
        }

        private ThingWithComps GetSelected()
        {
            return stockpileList.SelectedItem?.GetType().GetProperty("Item")?.GetValue(stockpileList.SelectedItem, null) as ThingWithComps;
        }

        private void UpdateForm(ThingWithComps selected)
        {
            if (selected == null)
                return;
            position.Text = selected.pos;
            stackCount.Value = selected.stackCount;
            if (selected.defObject != null)
            {
                itemCombo.SelectedIndex = itemCombo.Items.IndexOf(selected.defObject);
                if (selected.defObject.StackMax == null)
                {
                    stackCount.Visible = false;
                }
                else
                {
                    stackCount.Visible = true;
                    stackCount.Maximum = selected.defObject.StackMax.Value;
                }
            }
            else
            {
                stackCount.Visible = false;
                itemCombo.SelectedIndex = -1;
            }


        }

        private void stackCount_ValueChanged(object sender, EventArgs e)
        {
            ThingWithComps selected = GetSelected();
            selected.stackCount = (int)stackCount.Value;
        }

        private void itemCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ThingWithComps selected = GetSelected();
            if (selected == null)
                return;
            var objectDef = itemCombo.SelectedItem as ThingDef;
            selected.def = objectDef.DefName;

            UpdateForm(selected);
        }
    }
}
