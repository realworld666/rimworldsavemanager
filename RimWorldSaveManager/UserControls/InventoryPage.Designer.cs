﻿namespace RimWorldSaveManager.UserControls
{
    partial class InventoryPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.stockpileList = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.position = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.stackCount = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.itemCombo = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.stackCount)).BeginInit();
            this.SuspendLayout();
            // 
            // stockpileList
            // 
            this.stockpileList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)));
            this.stockpileList.FormattingEnabled = true;
            this.stockpileList.Location = new System.Drawing.Point(3, 5);
            this.stockpileList.Name = "stockpileList";
            this.stockpileList.Size = new System.Drawing.Size(200, 550);
            this.stockpileList.TabIndex = 0;
            this.stockpileList.SelectedIndexChanged += new System.EventHandler(this.stockpileList_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(212, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Position";
            // 
            // position
            // 
            this.position.Location = new System.Drawing.Point(286, 41);
            this.position.Name = "position";
            this.position.ReadOnly = true;
            this.position.Size = new System.Drawing.Size(200, 20);
            this.position.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(212, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Stack Count";
            // 
            // stackCount
            // 
            this.stackCount.Location = new System.Drawing.Point(286, 67);
            this.stackCount.Maximum = new decimal(new int[] {
            75,
            0,
            0,
            0});
            this.stackCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.stackCount.Name = "stackCount";
            this.stackCount.Size = new System.Drawing.Size(200, 20);
            this.stackCount.TabIndex = 5;
            this.stackCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.stackCount.ValueChanged += new System.EventHandler(this.stackCount_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(212, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Item";
            // 
            // itemCombo
            // 
            this.itemCombo.FormattingEnabled = true;
            this.itemCombo.Location = new System.Drawing.Point(286, 13);
            this.itemCombo.Name = "itemCombo";
            this.itemCombo.Size = new System.Drawing.Size(200, 21);
            this.itemCombo.TabIndex = 7;
            this.itemCombo.SelectedIndexChanged += new System.EventHandler(this.itemCombo_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(293, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Cannot Modify";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(215, 531);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Create new";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            // 
            // InventoryPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.stackCount);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.itemCombo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.position);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.stockpileList);
            this.Name = "InventoryPage";
            this.Size = new System.Drawing.Size(879, 557);
            ((System.ComponentModel.ISupportInitialize)(this.stackCount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox stockpileList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox position;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown stackCount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox itemCombo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
    }
}
